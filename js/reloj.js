function obtenerHora() {
    let fecha = new Date(); // Date es un objeto de js para obtener fecha y hora

    // traer las etiquetas html
    let diaSem = document.getElementById("diaSem"),
        numDia = document.getElementById("numDia"),
        mes = document.getElementById("mes"),
        anio = document.getElementById("anio"),
        hora = document.getElementById("hora"),
        min = document.getElementById("min"),
        seg = document.getElementById("seg"),
        amPM = document.getElementById("amPM");

    let diasSemana = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
        meses = ["Enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"]

    // asignar valor
    diaSem.innerText = diasSemana[fecha.getDay()];
    numDia.innerText = fecha.getDate();
    mes.innerText = meses[fecha.getMonth()];
    anio.innerText = fecha.getFullYear();

    // horas
    if (fecha.getHours() > 12) {
        if ((fecha.getHours() - 12) >= 10) {
            hora.innerText = (fecha.getHours() - 12);
        } else {
            hora.innerText = "0" + (fecha.getHours() - 12);
        }
    } else {
        if (fecha.getHours() < 10) {
            hora.innerText = "0" + fecha.getHours();
        } else {
            hora.innerText = fecha.getHours();
        }
    }

    // minutos
    if (fecha.getMinutes() < 10) {
        min.innerText = "0" + fecha.getMinutes();
    } else {
        min.innerText = fecha.getMinutes();
    }

    // segundos
    if (fecha.getSeconds() < 10) {
        seg.innerText = "0" + fecha.getSeconds();
    } else {
        seg.innerText = fecha.getSeconds();
    }

    // am pm
    if (fecha.getHours() >= 12) {
        amPM.innerText = "PM";
    } else {
        amPM.innerText = "AM";
    }

}

window.setInterval(obtenerHora, 1000);
obtenerHora()