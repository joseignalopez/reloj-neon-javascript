function saludar(){
    alert("holis mundis");
}

// window.setTimeout(saludar, 3000); // este método ejecuta una función con un retraso de X milisegundos
let tiempo = window.setInterval(contar, 1000); // este método ejecuta una función a intervalos de X milisegundos

let contador = 1;

function contar(){
    document.write(`${contador}<br>`);
    if(contador === 10){
        window.clearInterval(tiempo); //corta el setInterval especificado entre los paréntesis
    }
    contador++;
}

